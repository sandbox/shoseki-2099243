<?php

require_once('gameanalyticscom.logevents.inc');


function _gameanalyticscom_page() {

	$items['_gameanalyticscom_settings_form'] = drupal_get_form('_gameanalyticscom_settings_form');
	$items['_gameanalyticscom_test_api_form'] = drupal_get_form('_gameanalyticscom_test_api_form');
	return $items; 
}

function _gameanalyticscom_settings_form($form) {
	$form['gameanalyticscom_build_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Build ID'),
    '#default_value' => variable_get('gameanalyticscom_build_id', ''),
    '#description' => t(""),
  );
	$form['gameanalyticscom_game_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Game Key'),
    '#default_value' => variable_get('gameanalyticscom_game_key', ''),
    '#description' => t(""),
  );
	$form['gameanalyticscom_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => variable_get('gameanalyticscom_secret_key', ''),
    '#description' => t(""),
  );
  $form['gameanalyticscom_data_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('DATA API Key'),
    '#default_value' => variable_get('gameanalyticscom_data_api_key', ''),
    '#description' => t(""),
  );
  $form['gameanalyticscom_api_version'] = array(
    '#type' => 'textfield',
    '#title' => t('API Version'),
    '#default_value' => variable_get('gameanalyticscom_api_version', 1),
    '#description' => t(""),
  );

	return system_settings_form($form);
}

