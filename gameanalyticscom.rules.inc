<?php


function _gameanalyticscom_action_log_event_options_list() {
	$options = array(
		'design' => 'Design',
		'user' => 'User',
		'quality' => 'Quality',
		'business' => 'Business',
	);
	return $options;
}