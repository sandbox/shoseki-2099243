<?php
function _gameanalyticscom_get_generic_json_wrapper($user_id, $session_id) {
	
	$build_id = variable_get('gameanalyticscom_build_id', '1.0a');
	
	$json_message = array(
		'user_id' => '' . $user_id,
		'session_id' => $session_id,
		'build' => $build_id,
	);
	
	return $json_message;
	
}

function _gameanalyticscom_get_design_business_quality_fields_json_wrapper($event_id, $area = null, $x = null, $y = null, $z = null) {
	$json_message = array(
		'event_id' => $event_id,
	);
	
	if ($area != null) $json_message['area'] = $area; 
	if ($x != null && is_numeric($x)) $json_message['x'] = floatval($x);
	if ($y != null && is_numeric($y)) $json_message['y'] = floatval($y);
	if ($z != null && is_numeric($z)) $json_message['z'] = floatval($z);
	
	return $json_message;
}

function _gameanalyticscom_get_design_json_wrapper($user_id, $session_id, $event_id, $area = null, $x = null, $y = null, $z = null, $value = null) {

	$json_message = _gameanalyticscom_get_generic_json_wrapper($user_id, $session_id);
	$json_message += _gameanalyticscom_get_design_business_quality_fields_json_wrapper($event_id, $area = null, $x = null, $y = null, $z = null);
	
	if ($value != null) $json_message['value'] = $value;
	
	return $json_message;
}

function _gameanalyticscom_get_business_json_wrapper($user_id, $session_id, $event_id, $currency = 'GBP', $amount = 0, $area = null, $x = null, $y = null, $z = null) {

	$json_message = _gameanalyticscom_get_generic_json_wrapper($user_id, $session_id);
	$json_message += _gameanalyticscom_get_design_business_quality_fields_json_wrapper($event_id, $area = null, $x = null, $y = null, $z = null);
	
	$json_message['currency'] = $currency;
	$json_message['amount'] = $amount;
	
	return $json_message;
}

function _gameanalyticscom_get_quality_json_wrapper($user_id, $session_id, $event_id, $area = null, $x = null, $y = null, $z = null, $message = null) {

	$json_message = _gameanalyticscom_get_generic_json_wrapper($user_id, $session_id);
	$json_message += _gameanalyticscom_get_design_business_quality_fields_json_wrapper($event_id, $area = null, $x = null, $y = null, $z = null);

	if ($message != null) $json_message['message'] = $message;
	
	return $json_message;
}

function _gameanalyticscom_get_user_json_wrapper($user_id, $session_id, $gender = null, $birth_year = null, $friend_count = null) {
	$json_message = _gameanalyticscom_get_generic_json_wrapper($user_id, $session_id);

	if ($gender != null) $json_message['gender'] = $gender;
	if ($birth_year != null) $json_message['birth_year'] = $birth_year;
	if ($friend_count != null) $json_message['friend_count'] = $friend_count;
	
	return $json_message;
}