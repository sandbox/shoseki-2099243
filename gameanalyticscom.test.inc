<?php

function _gameanalyticscom_test_api_form($form, &$form_state) {

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Test API'),
		'#submit' => array('_gameanalyticscom_test_api_submit'),
	);
	return $form;
}

function _gameanalyticscom_test_api_submit($form, &$form_state) {

	global $user;
	$user_id = $user->uid;

	$session_id = session_id();

	$event_id = 'test';

	$area = 'test_area';

	$x = rand() / getrandmax();
	$y = rand() / getrandmax();
	$z = rand() / getrandmax();

  $value = rand() / getrandmax();

	$response_object = gameanalyticscom_log_design_event($user, $session_id, $event_id, $area, $x, $y, $z, $value);

	if (is_object($response_object)) {
		$response = sprintf('Response status : %s', $response_object->status);
	}
	else if (is_string($response_object)) {
		$response = sprintf('GameAnalytics.com returned : %s', $response_object);
	}
	else {
		$response = 'GameAnalytics.com response unavailable.';
	}

	drupal_set_message($response);
}