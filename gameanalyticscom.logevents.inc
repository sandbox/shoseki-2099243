<?php

require_once('gameanalyticscom.logevents.jsonwrappers.inc');

function _gameanalyticscom_get_url($category) {
	$gameanalyticscom_api_version = variable_get('gameanalyticscom_api_version', 1);
	$gameanalyticscom_game_key = variable_get('gameanalyticscom_game_key', '');
	$url = sprintf('http://api.gameanalytics.com/%s/%s/%s', $gameanalyticscom_api_version, $gameanalyticscom_game_key, $category);

	return $url;
}

function gameanalyticscom_log_design_event($user, $session_id, $event_id, $area = null, $x = null, $y = null, $z = null, $value = null) {
	$user_id = $user->uid;

	$request_json_object = _gameanalyticscom_get_design_json_wrapper($user_id, $session_id, $event_id, $area, $x, $y, $z, $value);

	$url = _gameanalyticscom_get_url('design');

	$response = _gameanalyticscom_make_curl_request($url, json_encode($request_json_object));
	$response_object = json_decode($response);
	return $response_object;
}

function gameanalyticscom_log_business_event($user, $session_id, $event_id, $currency = 'GBP', $amount = 0, $area = null, $x = null, $y = null, $z = null, $value = null) {
	$user_id = $user->uid;
	
	$request_json_object = _gameanalyticscom_get_business_json_wrapper($user_id, $session_id, $event_id, $currency, $amount, $area, $x, $y, $z);

	$url = _gameanalyticscom_get_url('business');

	$response = _gameanalyticscom_make_curl_request($url, json_encode($request_json_object));
	$response_object = json_decode($response);
	return $response_object;
}

function gameanalyticscom_log_quality_event($user, $session_id, $event_id, $area = null, $x = null, $y = null, $z = null, $value = null, $message = null) {
	$user_id = $user->uid;
	
	$request_json_object = _gameanalyticscom_get_business_json_wrapper($user_id, $session_id, $event_id, $area, $x, $y, $z, $message);

	$url = _gameanalyticscom_get_url('quality');

	$response = _gameanalyticscom_make_curl_request($url, json_encode($request_json_object));
	$response_object = json_decode($response);
	return $response_object;
}

function gameanalyticscom_log_user_event($user, $session_id, $gender = null, $birth_year = null, $friend_count = null) {
	$user_id = $user->uid;
	
	$request_json_object = _gameanalyticscom_get_user_json_wrapper($user_id, $session_id, $gender, $birth_year, $friend_count);

	$url = _gameanalyticscom_get_url('user');

	$response = _gameanalyticscom_make_curl_request($url, json_encode($request_json_object));
	$response_object = json_decode($response);
	return $response_object;
}

function _gameanalyticscom_make_curl_request($url, $data) {

	$gameanalyticscom_secret_key = variable_get('gameanalyticscom_secret_key', '');
	
	$data_md5 = md5($data . $gameanalyticscom_secret_key);

	$authorization_header = sprintf("Authorization: %s", $data_md5);
	$content_type_header = 'Content-Type: text/plain';
	
	global $user;

  $ch = curl_init();
  
  $headers = array($authorization_header);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  
  //curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
  $response = curl_exec($ch);

  // Check to see if any response whatsoever, and output debugging info
  if ($response === FALSE) {
    $curl_error = curl_error($ch);
    _gameanalyticscom_handle_error(10, check_plain('There was a problem using the GameAnalytics.com API, with the following message : ' . $curl_error));
  }
  elseif (strlen($response) == 0) {
    _gameanalyticscom_error(20, t('There was a problem using the GameAnalytics.com API, no data was returned.'));
  }
  curl_close($ch);

  if (in_array('administrator', $user->roles)) {
  	$api_object = array();
  	$api_object['url'] = $url;
  	$api_object['request'] = $data;  	
  	$api_object['response'] = $response;
  	$api_objects = array($api_object);
  	drupal_add_js(array('gameanalyticscom' => array('api_request' => $api_objects)), 'setting');
  }

  return $response;
}

function _gameanalyticscom_handle_error($error_code, $error_message, $drupal_set_message = false, $drupal_goto = true, $drupal_goto_url = null) {
  watchdog('gameanalyticscom', '' . $error_code . ' : ' . $error_message);

  global $user;
  if ($drupal_set_message || in_array('administrator', $user->roles)) drupal_set_message($error_message, 'warning');

  if ($drupal_goto) {
    if ($drupal_goto_url == null) {
      global $base_root;
      $drupal_goto_url = $base_root . '/404.php';
    }

    // Administrators don't need to be redirected, can use this to debug
    if (!in_array('administrator', $user->roles)) drupal_goto($drupal_goto_url);
  }
  
  if (in_array('administrator', $user->roles)) {
    	
    $error = array(
			'error_code' => $error_code,
			'error_message' => $error_message,
		  'drupal_set_message' => $drupal_set_message,
		  'drupal_goto' => $drupal_goto,
		  'drupal_goto_url' => $drupal_goto_url,
	  );
		
    $errors = array($error);
	  drupal_add_js(array('gameanalyticscom' => array('errors' => $errors)), 'setting');
  }
}